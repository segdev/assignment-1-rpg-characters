﻿using RPGCharacters.Attributes;
using RPGCharacters.Custom_Exceptions;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPGCharacters.Items.Armors;

namespace RPGCharacters.Characters
{
    public abstract class Characters
    {
        // Characters class basics properties
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttribute { get; set; }
        public PrimaryAttributes TotalPrimaryAttribute { get; set; }
        public SecondaryAttributes SecondaryAttribute { get; set; }
        public Dictionary<Slot, Item> ItemInventory { get; set; }

        // Overloaded constructor that creates character with name, level set to one and empty item inventory dictionary
        public Characters(string name)
        {
            Name = name;
            Level = 1;
            ItemInventory = new Dictionary<Slot, Item>();
            InitializeInventory();  // Item inventory (dictionary) is initialized
        }

        /// <summary>
        /// Initializes inventory dictionary by creating as many slots as there is for items
        /// and one weapon (5 slots) and making them null
        /// </summary>
        public void InitializeInventory()
        {
            // creates empty slot for every enum value in Item.Slots 
            foreach(Slot slot in Enum.GetValues(typeof(Slot)))
            {
                // Initializes slot with null value
                ItemInventory.Add(slot, null);
            }
        }

        /// <summary>
        /// Method for leveling up the character once every time methos is called. 
        /// Method increases primary and secondary attributes.
        /// </summary>
        public abstract void LevelUp();


        /// <summary>
        /// Method for leveling up the character multiple times in one call.
        /// Method increases primary and secondary attributes as many times.
        /// </summary>
        /// <param name="times">Amount of levels character is gonna be leveled up</param>
        public abstract void LevelUp(int times);

        /// <summary>
        /// Method for updating characters DPS.
        /// Method is called everytime armor or weapon is equipped.
        /// </summary>
        public abstract void UpdateDPS();


        /// <summary>
        /// Method that tells characters stats. 
        /// </summary>
        /// <returns>Returns string with stats</returns>
        public string TellMeStats()
        {
            string stats = $"{Name}'s stats: || Lvl: {Level} | Strength: {TotalPrimaryAttribute.Strength} |" +
                $" Dexterity: {TotalPrimaryAttribute.Dexterity} | Intelligence: {TotalPrimaryAttribute.Intelligence} |" +
                $" HP: {SecondaryAttribute.Healt} | Armor Rating: {SecondaryAttribute.ArmorRating} |" +
                $" Elemental Resistance: {SecondaryAttribute.ElementResistance} | DPS: {SecondaryAttribute.DPS} ||";
            return stats;
        }
    }
}
