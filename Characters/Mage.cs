﻿using RPGCharacters.Attributes;
using RPGCharacters.Custom_Exceptions;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPGCharacters.Items.Armors;

namespace RPGCharacters.Characters
{
    public class Mage : Characters
    {
        // Get only property which is initialized with characters starting primary attributes
        public PrimaryAttributes StartingAttributes { get; } = new PrimaryAttributes()
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 8,
            Vitality = 5
        };

        // Get only property which is initialized with characters primary attributes for leveling up
        public PrimaryAttributes LevelUpAttributes { get; } = new PrimaryAttributes()
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 5,
            Vitality = 3
        };

        // Array that holds information of weapon types that are allowed for the character
        readonly WeaponTypes[] possibleWeaponTypes = new WeaponTypes[] { 
            WeaponTypes.WEAPON_STAFF, WeaponTypes.WEAPON_WAND };

        // Array that holds information of armor types that are allowed for the character
        readonly ArmorTypes[] possibleArmorTypes = new ArmorTypes[] { 
            ArmorTypes.ARMOR_CLOTH };

        // Basic overloaded constructor for creating a mage character. Parameter name is passed to the parent’s constructor.
        public Mage(string name) : base(name)
        {
            BasePrimaryAttribute = new PrimaryAttributes(StartingAttributes);
            TotalPrimaryAttribute = new PrimaryAttributes(StartingAttributes);
            SecondaryAttribute = new SecondaryAttributes(TotalPrimaryAttribute);
        }

        /// <summary>
        /// Method for leveling up the character once every time methos is called. 
        /// Method increases primary and secondary attributes.
        /// </summary>
        public override void LevelUp()
        {

            BasePrimaryAttribute.LevelUp(LevelUpAttributes);
            TotalPrimaryAttribute.LevelUp(LevelUpAttributes);
            SecondaryAttribute.LevelUp(TotalPrimaryAttribute);
            Level += 1;
        }

        /// <summary>
        /// Method for leveling up the character multiple times in one call.
        /// Method increases primary and secondary attributes as many times.
        /// </summary>
        /// <param name="times">Amount of levels character is gonna be leveled up</param>
        public override void LevelUp(int times)
        {
            // First checks that parameter input is not zero or negative integer
            if (times <= 0)
                // If times is not positive int, excepition ArgumentException is awaked
                throw new ArgumentException();

            BasePrimaryAttribute.LevelUp(LevelUpAttributes, times);
            TotalPrimaryAttribute.LevelUp(LevelUpAttributes, times);
            SecondaryAttribute.LevelUp(TotalPrimaryAttribute);
            Level += 1 * times;
        }

        /// <summary>
        /// Method for equipping weapon to the item inventory dictionary.
        /// </summary>
        /// <param name="weapon">Object weapon holds information of weapon that is equipped</param>
        /// <returns>String message is returned if weapon is equipped successfully</returns>
        /// <exception cref="InvalidWeaponException">Exception is awaked if weapons level is higher than characters
        /// or if weapons type is not suitable for character</exception>
        public string EquipItem(Weapons weapon)
        {
            // Checks if characters level is at least same as weapons
            if (Level < weapon.ItemLevel)
                // Exception ia thrown if characters level is lower than weapons
                throw new InvalidWeaponException("Weapons level is too high for your character!");
            // If level is ok, this checks if weapons type is appropriate for the character
            if (!possibleWeaponTypes.Contains(weapon.WeaponType))
                // Exception ia thrown if weapon is not suitable for the character
                throw new InvalidWeaponException("Weapons type is wrong for your character!");
            // Adds weapon to the item inventory in the slot for weapon. Can hold only one weapon at the time
            ItemInventory[Slot.SLOT_WEAPON] = weapon;
            // After adding calls UptadeDPS method to update characters DPS
            UpdateDPS();

            // Returns success string if exceptions were not thrown
            return "New weapon equipped!";
        }

        /// <summary>
        /// Method for equipping armor to the item inventory dictionary.
        /// </summary>
        /// <param name="armour">Object armour holds information of armor that is equipped</param>
        /// <param name="slot">Tells to which slot item is equipped</param>
        /// <returns>String message is returned if armor is equipped successfully</returns>
        /// <exception cref="InvalidArmorException">Exception is awaked if armors level is higher than characters
        /// or if armors type is not suitable for character</exception>
        public string EquipItem(Armors armour, Slot slot)
        {
            // Checks if characters level is at least same as armors
            if (Level < armour.ItemLevel)
                // Exception ia thrown if characters level is lower than armors
                throw new InvalidArmorException("Armors level is too high for your character!");
            // If level is ok, this checks if armors type is appropriate for the character
            if (!possibleArmorTypes.Contains(armour.ArmorType))
                // Exception ia thrown if armor is not suitable for the character
                throw new InvalidArmorException("Armors type is wrong for your character!");
            // Adds armor to the item inventory in the slot that was given in the parameters. One slot can hold only one armor at the time
            ItemInventory[slot] = armour;
            // After equipping armor this updates total primary atrributes and adds armors strength and
            // vitality attributes to characters own strength and vitality
            TotalPrimaryAttribute.Strength += armour.ArmorAttributes.Strength;
            TotalPrimaryAttribute.Vitality += armour.ArmorAttributes.Vitality;
            // After adding calls UptadeDPS method to update characters DPS
            UpdateDPS();

            // Returns success string if exceptions were not thrown
            return "New armour equipped!";
        }

        /// <summary>
        /// Method for updating characters DPS.
        /// Method is called everytime armor or weapon is equipped.
        /// </summary>
        public override void UpdateDPS()
        {
            // First adds weapon from characters item inventory into the variable item
            Weapons item = (Weapons)ItemInventory[Slot.SLOT_WEAPON];
            // Then checks if there was any weapon or was slot empty
            if (item == null)
                // If weapon slot was empty charactes DPS is calculated using 1 as weapons DPS
                SecondaryAttribute.DPS = (1 * Convert.ToDouble(1 + (TotalPrimaryAttribute.Intelligence / 100)));
            else
                // If there was a weapon in the slot DPS is calculated with weapons DPS and with right total primary attribute
                SecondaryAttribute.DPS = (item.WeaponAttribute.DPS * Convert.ToDouble(1 + (TotalPrimaryAttribute.Intelligence / 100)));
        }
    }
}
