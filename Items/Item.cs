﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    public class Item
    {  
        // Classes properties
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public Slot ItemSlot { get; set; }
    }

    // Enum Slot that holds all values for possible slots to store an item
    public enum Slot
    {
        SLOT_HEAD,      // Slot only for armor
        SLOT_BODY,      // Slot only for armor
        SLOT_LEGS,      // Slot only for armor
        SLOT_WEAPON     // Slot only for weapon
    }
}
