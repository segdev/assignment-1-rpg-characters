﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    public class Weapons : Item
    {  
        // Child classes properties
        public WeaponTypes WeaponType { get; set; }
        public WeaponAttributes WeaponAttribute { get; set; }

        // Overloaded constructor for creating Weapons
        public Weapons(string name, int minLevel, Slot itemSlot, WeaponTypes weapon, WeaponAttributes weaponAttributes)
        {
            ItemName = name;
            ItemLevel = minLevel;
            ItemSlot = itemSlot;
            WeaponType = weapon;
            WeaponAttribute = weaponAttributes;
        }

        // // Another overloaded constructor for easier creation of new Weapons objects
        public Weapons()
        {

        }
    }

    // Enum WeaponTypes that holds all possible weapon types
    public enum WeaponTypes
    {
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }

}
