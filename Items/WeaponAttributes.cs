﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    public class WeaponAttributes
    {
        // Child class attribute properties
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS => Convert.ToDouble(Damage) * AttackSpeed;

        // Overloaded constructor for creating WeaponAttributes
        public WeaponAttributes(int damage, double speed)
        {
            Damage = damage;
            AttackSpeed = speed;
        }

        // Another overloaded constructor for easier creation of new WeaponAttributes objects
        public WeaponAttributes()
        {

        }
    }
}
