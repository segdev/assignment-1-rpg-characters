﻿using RPGCharacters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    public class Armors : Item
    {
        // Child classes properties. ArmorAttributes uses same class as type as base and total primary attributes
        public PrimaryAttributes ArmorAttributes { get; set; }
        public ArmorTypes ArmorType { get; set; }   // enum type property

        // Overloaded constructor for creating Armors
        public Armors(string name, int level, Slot slot, ArmorTypes armorType, PrimaryAttributes armorAttributes)
        {
            ItemName = name;
            ItemLevel = level;
            ItemSlot = slot;
            ArmorType = armorType;
            ArmorAttributes = armorAttributes;
        }

        // Another overloaded constructor for easier creation of new Armors objects
        public Armors()
        {

        }

        // Enum ArmorTypes that holds all possible armor types
        public enum ArmorTypes
        {
            ARMOR_CLOTH,
            ARMOR_LEATHER,
            ARMOR_MAIL,
            ARMOR_PLATE
        }
    }
}
