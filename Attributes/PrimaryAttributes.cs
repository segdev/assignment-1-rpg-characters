﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    public class PrimaryAttributes
    {
        // Properties of the class
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        // Overloaded constructor that takes only integres as a parameters
        public PrimaryAttributes(int strength, int dexterity, int intelligence, int vitality)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            Vitality = vitality;
        }

        // Overloaded constructor that takes PrimaryAttributes class object as a parameter
        public PrimaryAttributes(PrimaryAttributes attributes)
        {
            Strength = attributes.Strength;
            Dexterity = attributes.Dexterity;
            Intelligence = attributes.Intelligence;
            Vitality = attributes.Vitality;
        }

        // Overloaded constructor that takes nothing as a parameter
        public PrimaryAttributes()
        {
        }


        /// <summary>
        /// Method to level up the character once.
        /// </summary>
        /// <param name="attributes"> Method takes PrimaryAttributes object as a parameter that
        /// stores all the attributes</param>
        public void LevelUp(PrimaryAttributes attributes)
        {
            Strength += attributes.Strength;
            Dexterity += attributes.Dexterity;
            Intelligence += attributes.Intelligence;
            Vitality += attributes.Vitality;

        }

        /// <summary>
        /// Method to level up the character multiple times at once.
        /// </summary>
        /// <param name="attributes">Item takes PrimaryAttributes object as a parameter that
        /// stores all the attributes</param>
        /// <param name="times">Integer that tells how many times character is gonna be leveled up</param>
        public void LevelUp(PrimaryAttributes attributes, int times)
        {
            Strength += attributes.Strength*times;
            Dexterity += attributes.Dexterity*times;
            Intelligence += attributes.Intelligence*times;
            Vitality += attributes.Vitality*times;

        }
    }
}
