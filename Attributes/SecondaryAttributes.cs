﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    public class SecondaryAttributes
    {
        // Properties of the class
        public int Healt { get; set; }
        public int ArmorRating { get; set; }
        public int ElementResistance { get; set; }
        public double DPS { get; set; }

        // Overloaded constructor that takes PrimaryAttributes class object as a parameter
        public SecondaryAttributes(PrimaryAttributes totalAttributes)
        {
            Healt = totalAttributes.Vitality * 10 ;
            ArmorRating = totalAttributes.Strength + totalAttributes.Dexterity;
            ElementResistance = totalAttributes.Intelligence;
            DPS = 1 * Convert.ToDouble(1 + (totalAttributes.Strength / 100));
        }

        /// <summary>
        /// Method that updates properties when character level up's
        /// </summary>
        /// <param name="attributes">Method takes PrimaryAttributes object as a parameter that
        /// stores all the attributes</param>
        public void LevelUp(PrimaryAttributes attributes)
        {
            Healt = attributes.Vitality * 10;
            ArmorRating = attributes.Strength + attributes.Dexterity;
            ElementResistance = attributes.Intelligence;
        }
       
    }
}
