﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Custom_Exceptions
{
    public class InvalidArmorException : Exception
    {
        // Default overloaded constructor
        public InvalidArmorException()
        {

        }
        // Overloaded constructor that overrides error message with the message we pass in
        public InvalidArmorException(string message) : base(message)
        {
        }
        // Overriding Message property with our own custom message
        public override string Message => "Invalid armor selection";
    }
}
