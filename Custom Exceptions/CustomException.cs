﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Custom_Exceptions
{
    public class CustomException : Exception
    {
        // Default overloaded constructor
        public CustomException()
        {

        }
        // Overloaded constructor that overrides error message with the message we pass in
        public CustomException(string message) : base(message)
        {
        }
        // Overriding Message property with our own custom message
        public override string Message => "Throwing some custom error";
    }
}
